class UsersController < ApplicationController
    
    def index
        @users=User.all
    end
    
    def new
        @user=User.new
    end
    
    def create
        @user=User.new(uid: params[:uid],pass: params[:pass])
        if @user.save
            flash[:notice] = "ユーザー登録が完了しました"
            redirect_to("/users/index")
        else
            render("users/new")
        end
    end
    
    def destroy
        
    end
    
    
    def login
        @user = User.find_by(uid: params[:uid], pass: params[:pass])
  
         if @user
            session[:uid] = @user.uid
            flash[:notice] = "ログインしました"
             redirect_to("/users/index")
         else
             redirect_to("/signup")
         end
    end
    
    
    def logout
       session[:uid]=nil
       redirect_to("/users/index")
    end
end
