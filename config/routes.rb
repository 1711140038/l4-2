Rails.application.routes.draw do
  get 'top/main'
  post "top/login"
  root "top#main"
  get "login" => "users#login_form"
  post "users/login" => "users#login"
  post "users/logout" => "users#logout"
  post "users/create" => "users#create"
  get "users/index" => "users#index"
  get "signup" => "users#new"
  get "logout" => "users#logout"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
