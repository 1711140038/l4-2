require 'test_helper'

class UserControllerTest < ActionDispatch::IntegrationTest
  test "should get uid:string" do
    get user_uid:string_url
    assert_response :success
  end

  test "should get pass:string" do
    get user_pass:string_url
    assert_response :success
  end

end
